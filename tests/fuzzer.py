#!/usr/bin/python2.7
# Copyright 2013 Nikita Kazeev
import itertools
import cPickle

# The idea is very simple
# 1. Try all k1/k2/M/P combinations
# 2. Detect dublicates
# 3. Everything not dublicate is good, everythig else is 0

def generate_house(MAX):
    the_house = dict()
    for n, P, M in itertools.product(
        xrange(1, MAX+1), xrange(1, MAX+1), xrange(1, MAX+1)):
        print("Processing n=%d; P=%d; M=%d" % (n, P, M))
        for Ks in itertools.product(
            xrange(1, n*M*(P + 1) + 1), xrange(1, n*M*P + 1)):
            # P + 1
            # 17  2  4   1   2
            alphas = map(lambda k: (k - 1) // n, Ks)
            Ps = map(lambda alpha: (alpha // M) + 1, alphas)
            Ns = map(lambda alpha: (alpha % M) + 1, alphas)

            in_data = (Ks[0], M, Ks[1], Ps[1], Ns[1])
            out_data = [Ps[0], Ns[0]]
            # if we have already seen the input combined with a
            # different output, it's ambigious
            if in_data in the_house.keys():
                the_house[in_data][0] = map(lambda (x, y): x if x == y else 0,
                                           itertools.izip(the_house[in_data][0],
                                                    out_data))
                the_house[in_data][1].add(n)
            else:
                the_house[in_data] = [out_data, {n}]
    return the_house

def validator(in_data, out_data, n, MAX):
    """The function to disacard false positivs: the cases, that should be
    ambiguent, but are not, becouse of finitness of the checked M, P, n"""
    p1, n1 = out_data
    if p1 == 0 and n1 == 0:
        return True
    # OK, now partially ambigious data goes
    k1, m, k2, p2, n2 = in_data

    # general MAX - 1 boundaries
    res_gen = all(map(lambda x: x < MAX, [max(n), p1]))

    # the case of the last floor ambigety
    # 10  2  6   1   2
    # idea: if n increases for more than 2 and that would move
    # K1 to the previous doorway, will that be covered?
    # res_doorway_ambig = (p1 == 1 or n1 != 1 or (
    # (k1 - max(n)*m) <= m))
    # return (len(n) == 1)
    #    return res_gen and res_doorway_ambig
    return res_gen

if __name__ == "__main__":
    pickled = False
    try:
        infile = open("house.db", 'rb')
        MAX = cPickle.load(infile)
        house = cPickle.load(infile)
        infile.close()
        pickled = True
        print("Successfully loaded house.db")
    except IOError:
        print("Error loading house.db")

    if not pickled:
        MAX = 5
        house = generate_house(MAX)
        outfile = open("house.db", 'wb')
        cPickle.dump(MAX, outfile)
        cPickle.dump(house, outfile)
        outfile.close()

    for in_data, [out_data, n] in itertools.ifilter(
        lambda data: validator(data[0], data[1][0], data[1][1], MAX),
        house.iteritems()):

        filename = "fuzz_k1%d_M%d_k2%d_p2%d_n2%d.test" % in_data
        outfile = open(filename, 'w')
        outfile.write("%d %d %d %d %d\n" % in_data)
        outfile.write("%d %d" % tuple(out_data))
        outfile.close()
