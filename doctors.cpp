// Copyright 2013 Nikita Kazeev
#include<stdio.h>
#include<string.h>
#define min(a, b) (((a) < (b))?(a):(b))

// The working function
// Takes arguments as prescribed in the task
// Returns 0 on success
// -1 on general contradiction
// -2 on ambiguity
// -3 if flats per platform is not int
// Sets p1, n1 to the answer if success,
// to 0 if ambiguous, to -1 if contradiction
// CONSIDER remove WTF
// -42 on WTF

int find_num(unsigned int k1, unsigned int m, unsigned int k2,
             unsigned int p2, unsigned int n2, int* p1, int* n1) {
  // First, contradictions check
  // NOTE(kazeevn) we assume the task to be correct
  // and the numbers to be greater than zero
  if (n2 > m || k2 < n2 + m * (p2 - 1)) {
    // the known flat must be on valid floor
    // and there can't be less than one flat per floor
    *p1 = -1;
    *n1 = -1;
    return -1;
  }
  // Next, if we are asked about the same flat,
  // we know exactly, no matter what
  if (k1 == k2) {
    *p1 = p2;
    *n1 = n2;
    return 0;
  }
  // Then easy cases end :(
  // It will be cleaner to start
  // numbering from 0
  // Wonder, why don't they do this IRL?
  --k1;
  --k2;
  --p2;
  --n2;

  // alpha - number of full platforms before
  unsigned int alpha2 = n2 + m * p2;
  unsigned int n_upper, n_lower;
  // both limits are inclusive
  if (!alpha2) {
    n_lower = k2 + 1;
    n_upper = 42e3;
    // The upper estimate is correct in terms of the limits of the problem
    // n_max would be reached under following conditions: k2 = max, p2, n2 = min
    // => n_max <= 1000
  } else {
    n_upper = k2 / alpha2;
    n_lower = (k2 + 1) / (alpha2 + 1);
    if ((k2 + 1) % (alpha2 + 1))
      ++n_lower;
  }
  printf("k2 %u, alpha2 %u, n %u %u\n", k2, alpha2, n_lower, n_upper);

  // Now, having extracted as much information about n,
  // as possible, we try to find P1 and N1
  if (n_upper < n_lower) {
    // n is not integer
    if (n_lower - n_upper != 1) {
      printf("WTF!?!\n");
      return -42;
    }
    *p1 = -1;
    *n1 = -1;
    return -3;
  } else if (n_lower == n_upper) {
    // n is unique
    unsigned int alpha1 = k1 / n_lower;
    *p1 = (alpha1 / m) + 1;
    *n1 = (alpha1 % m) + 1;
    return 0;
  } else {
    // We have an estimate
    unsigned int alpha1_lower = k1 / n_upper;
    unsigned int p1_lower = (alpha1_lower / m) + 1;

    unsigned int alpha1_upper = k1 / n_lower;
    unsigned int p1_upper = (alpha1_upper / m) + 1;

    if (p1_lower == p1_upper)
      *p1 = p1_lower;
    else
      *p1 = 0;

    // such examples as
    // 11  2  4   1   2
    // teach us that a floor sometimes can be determined
    // without knowing the doorway
    if (m == 1) {
      *n1 = 1;
    } else {
      unsigned int n1_estimate = (alpha1_lower % m) + 1;
      for (unsigned int n_est = n_lower; n_est <= n_upper; ++n_est) {
        // we cycle over possible n values and try estimating the floor
        // if all the estimates are equal, we can safely return them
        if (((k1 / n_est) % m) + 1 != n1_estimate) {
          n1_estimate = 0;
          break;
        }
        if (k1 / n_est == 0) {
          // There is no point in increasing n_est even further
          break;
        }
      }
      *n1 = n1_estimate;
    }

    if (!n1 && !p1)
      return 0;
    else
      return -2;
  }

  // The tail. If something ever goes wrong up here,
  // we'll notice
  *p1 = -42;
  *n1 = -42;
  return -42;
}

int main(int argc, char** argv) {
  unsigned int k1, m, k2, p2, n2;
  int p1, n1, p1_test, n1_test;
  FILE* infile = NULL;
  char* infile_name = NULL;
  bool test_run = false;
  // In a test run we assume the file to contain two
  // lines - data and the answer. If it doesn't...
  if (argc < 2) {
    printf("Usage: doctors [-d] input_file\n");
    return -1;
  }
  if ((argc == 3) && !strcmp(argv[1], "-d")) {
    test_run = true;
    infile_name = argv[2];
  } else {
    infile_name = argv[1];
  }
  infile = fopen(infile_name, "r");
  if (!infile) {
    printf("Error opening file %s\n", infile_name);
    return -2;
  }
  int err = fscanf(infile, "%u %u %u %u %u", &k1, &m, &k2, &p2, &n2);
  if (err != 5) {
    printf("Invalid data file\n");
    return -3;
  }
  if (test_run) {
    err = fscanf(infile, "%u %u", &p1_test, &n1_test);
    if (err != 2) {
      printf("Invalid test data in the file\n");
      return -3;
    }
  }
  err = fclose(infile);
  if (err)
    printf("Failed to close the file...");

  int res = find_num(k1, m, k2, p2, n2, &p1, &n1);
  if (!test_run) {
    printf("%d %d\n", p1, n2);
    return 0;
  } else {
    printf("test: %d %d\nresult: %d %d\n", p1_test, n1_test, p1, n1);
    if (p1 == p1_test && n1 == n1_test) {
      printf("PASSED\n");
      return 0;
    } else {
      printf("FAILED\n");
      if (res != 0)
        return res;
      else
        return -100;
    }
  }
}

