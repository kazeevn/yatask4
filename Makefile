doctors: doctors.cpp
	g++ -O2 -Wall -Werror -o doctors doctors.cpp
%.test: FORCE doctors
	@echo "=+=+= Processing $@ =+=+="
	@(echo "K1 M K2 P2 N2"; head -n 1 $@) | column -t
	@./doctors -d $@
fuzz: FORCE
	cd tests && python fuzzer.py
defuzz: FORCE
	rm tests/fuz*.test
FORCE:
